# Copyright (C) 2011 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# TODO(b/147336716): Remove BUILD_BROKEN_PREBUILT_ELF_FILES
BUILD_BROKEN_PREBUILT_ELF_FILES := true
BUILD_BROKEN_ELF_PREBUILT_PRODUCT_COPY_FILES := true

# Should be uncommented after fixing vndk-sp violation is fixed.
PRODUCT_FULL_TREBLE_OVERRIDE := true

TARGET_BOARD_INFO_FILE := device/samsung/universal3830/board-info.txt

ifneq ($(TARGET_PLATFORM_32BIT), true)
TARGET_ARCH := arm64
TARGET_ARCH_VARIANT := armv8-a
TARGET_CPU_ABI := arm64-v8a
TARGET_CPU_VARIANT := generic

TARGET_2ND_ARCH := arm
TARGET_2ND_ARCH_VARIANT := armv7-a-neon
TARGET_2ND_CPU_ABI := armeabi-v7a
TARGET_2ND_CPU_ABI2 := armeabi
TARGET_2ND_CPU_VARIANT := cortex-a15
else
TARGET_ARCH := arm
TARGET_ARCH_VARIANT := armv7-a-neon
TARGET_CPU_ABI := armeabi-v7a
TARGET_CPU_ABI2 := armeabi
TARGET_CPU_VARIANT := cortex-a15
endif

TARGET_CPU_SMP := true
TARGET_NO_BOOTLOADER := true
TARGET_NO_RADIOIMAGE := true

TARGET_DEVICE_NAME := universal3830
TARGET_SOC_NAME := exynos

# bionic libc options
#ARCH_ARM_USE_MEMCPY_ALIGNMENT := true
#BOARD_MEMCPY_ALIGNMENT := 64
#BOARD_MEMCPY_ALIGN_BOUND := 32768
BOARD_MEMCPY_AARCH32 := true
BOARD_MEMCPY_MNGS := true

# SMDK common modules
BOARD_SMDK_COMMON_MODULES := liblight

OVERRIDE_RS_DRIVER := libRSDriverArm.so
BOARD_EGL_CFG := device/samsung/universal3830/conf/egl.cfg
#BOARD_USES_HGL := true
USE_OPENGL_RENDERER := true
NUM_FRAMEBUFFER_SURFACE_BUFFERS := 3
BOARD_USES_EXYNOS5_COMMON_GRALLOC := true
ifeq ($(ENABLE_S_FEATURE),true)
BOARD_USES_EXYNOS_GRALLOC_VERSION := 4
else ifeq ($(ENABLE_R_FEATURE),true)
BOARD_USES_EXYNOS_GRALLOC_VERSION := 4
else
BOARD_USES_EXYNOS_GRALLOC_VERSION := 3
endif
BOARD_USES_ALIGN_RESTRICTION := true
BOARD_USES_GRALLOC_ION_SYNC := true

# Graphics
BOARD_USES_EXYNOS_DATASPACE_FEATURE := true

ifeq ($(ENABLE_S_FEATURE),true)
BOARD_USES_GRALLOC_SCALER_WFD := true
else ifeq ($(ENABLE_R_FEATURE),true)
BOARD_USES_GRALLOC_SCALER_WFD := true
endif

# Storage options
BOARD_USES_SDMMC_BOOT := false
BOARD_USES_UFS_BOOT := false
BOARD_USES_VENDORIMAGE := true
TARGET_COPY_OUT_VENDOR := vendor
BOARD_VENDORIMAGE_FILE_SYSTEM_TYPE := ext4
BOARD_VENDORIMAGE_PARTITION_SIZE := 314572800

TARGET_USERIMAGES_USE_F2FS := true
TARGET_USERIMAGES_USE_EXT4 := true
#BOARD_SYSTEMIMAGE_FILE_SYSTEM_TYPE :=ext4
BOARD_SYSTEMIMAGE_PARTITION_SIZE := 1572864000
BOARD_USERDATAIMAGE_PARTITION_SIZE := 11796480000
BOARD_CACHEIMAGE_FILE_SYSTEM_TYPE := ext4
BOARD_CACHEIMAGE_PARTITION_SIZE := 69206016
#BOARD_FLASH_BLOCK_SIZE := 512
BOARD_FLASH_BLOCK_SIZE := 4096
BOARD_MOUNT_SDCARD_RW := true

########################
# Video Codec
########################
# 0. Default C2
BOARD_USE_DEFAULT_SERVICE := true

# 2. Exynos OMX
BOARD_USE_DMA_BUF := true
BOARD_USE_NON_CACHED_GRAPHICBUFFER := true
BOARD_USE_GSC_RGB_ENCODER := true
BOARD_USE_CSC_HW := false
BOARD_USE_S3D_SUPPORT := false
BOARD_USE_DEINTERLACING_SUPPORT := true
BOARD_USE_HEVCENC_SUPPORT := true
BOARD_USE_HEVC_HWIP := false
BOARD_USE_VP9DEC_SUPPORT := false
BOARD_USE_VP9ENC_SUPPORT := false
BOARD_USE_WFDENC_SUPPORT := false
BOARD_USE_CUSTOM_COMPONENT_SUPPORT := true
BOARD_USE_VIDEO_EXT_FOR_WFD_HDCP := true
BOARD_USE_SINGLE_PLANE_IN_DRM := true
BOARD_USE_WA_ION_BUF_REF := true
BOARD_USE_SMALL_SECURE_MEMORY := true
########################

#
# AUDIO & VOICE
#
BOARD_USES_GENERIC_AUDIO := true

# Primary AudioHAL Configuration
BOARD_USE_COMMON_AUDIOHAL := true
BOARD_USE_CALLIOPE_AUDIOHAL := false
BOARD_USE_AUDIOHAL := false
BOARD_USE_AUDIOHAL_COMV1 := true

# SoundTriggerHAL Configuration
BOARD_USE_SOUNDTRIGGER_HAL := false
BOARD_USE_SOUNDTRIGGER_HAL_MMAP := false

# CAMERA
BOARD_BACK_CAMERA_ROTATION := 90
BOARD_FRONT_CAMERA_ROTATION := 270
BOARD_SECURE_CAMERA_ROTATION := 0
BOARD_BACK_CAMERA_SENSOR := SENSOR_NAME_S5K2P7SQ
#BOARD_BACK_CAMERA_SENSOR := SENSOR_NAME_S5KGM1SP
BOARD_FRONT_CAMERA_SENSOR := SENSOR_NAME_S5K6B2
#BOARD_FRONT_CAMERA_SENSOR := SENSOR_NAME_S5K2X5SP
BOARD_SECURE_CAMERA_SENSOR := SENSOR_NAME_NOTHING

#BOARD_BACK_1_CAMERA_SENSOR := SENSOR_NAME_S5K5E9
BOARD_BACK_1_CAMERA_SENSOR_OPEN := false
BOARD_BACK_1_CAMERA_SENSOR := SENSOR_NAME_S5K2T7SX
BOARD_FRONT_1_CAMERA_SENSOR := SENSOR_NAME_NOTHING
BOARD_BACK_2_CAMERA_SENSOR := SENSOR_NAME_NOTHING
BOARD_BACK_2_CAMERA_SENSOR_OPEN := false
BOARD_FRONT_2_CAMERA_SENSOR := SENSOR_NAME_NOTHING
BOARD_BACK_3_CAMERA_SENSOR := SENSOR_NAME_NOTHING
BOARD_FRONT_3_CAMERA_SENSOR := SENSOR_NAME_NOTHING

BOARD_CAMERA_USES_DUAL_CAMERA := true
BOARD_DUAL_CAMERA_REAR_ZOOM_MASTER := CAMERA_ID_BACK_0
BOARD_DUAL_CAMERA_REAR_ZOOM_SLAVE := CAMERA_ID_BACK_1
BOARD_DUAL_CAMERA_REAR_PORTRAIT_MASTER := CAMERA_ID_BACK_1
BOARD_DUAL_CAMERA_REAR_PORTRAIT_SLAVE := CAMERA_ID_BACK_0
BOARD_DUAL_CAMERA_FRONT_PORTRAIT_MASTER := CAMERA_ID_FRONT_0
BOARD_DUAL_CAMERA_FRONT_PORTRAIT_SLAVE := CAMERA_ID_FRONT_1

BOARD_CAMERA_USES_DUAL_CAMERA_SOLUTION_FAKE := false
BOARD_CAMERA_USES_DUAL_CAMERA_SOLUTION_ARCSOFT := false

#BOARD_CAMERA_USES_LLS_SOLUTION := true
ifeq ($(TARGET_PLATFORM_32BIT), true)
#BOARD_CAMERA_USES_CAMERA_SOLUTION_VDIS := true
else
BOARD_CAMERA_USES_CAMERA_SOLUTION_VDIS := true
endif
BOARD_CAMERA_USES_SLSI_PLUGIN := true
#BOARD_CAMERA_BUILD_SOLUTION_SRC := true
BOARD_CAMERA_USES_PIPE_HANDLER := true
BOARD_CAMERA_USES_HIFI_LLS_CAPTURE := true
BOARD_CAMERA_USES_HIFI_CAPTURE := true
BOARD_CAMERA_USES_SLSI_VENDOR_TAGS := true
BOARD_CAMERA_USES_REMOSAIC_SENSOR := false
#BOARD_CAMERA_USES_SENSOR_LISTENER := true
BOARD_CAMERA_USES_SENSOR_GYRO_FACTORY_MODE := true

# HWComposer
BOARD_HWC_VERSION := libhwc2.1
TARGET_RUNNING_WITHOUT_SYNC_FRAMEWORK := false
BOARD_HDMI_INCAPABLE := true
TARGET_USES_HWC2 := true
HWC_SKIP_VALIDATE := true
HWC_SUPPORT_COLOR_TRANSFORM := false
TARGET_HAS_WIDE_COLOR_DISPLAY := false
TARGET_USES_DISPLAY_RENDER_INTENTS := false
#BOARD_USES_DISPLAYPORT := true
#BOARD_USES_EXTERNAL_DISPLAY_POWERMODE := true
BOARD_USES_EXYNOS_AFBC_FEATURE := false
#BOARD_USES_HDRUI_GLES_CONVERSION := true
BOARD_USES_HWC_SERVICES := true
VSYNC_EVENT_PHASE_OFFSET_NS := 0
SF_VSYNC_EVENT_PHASE_OFFSET_NS := 0

# WifiDisplay
BOARD_USES_VIRTUAL_DISPLAY := true
BOARD_USES_DISABLE_COMPOSITIONTYPE_GLES := true
BOARD_USES_SECURE_ENCODER_ONLY := true
BOARD_USES_CONTIG_MEMORY_FOR_SCRATCH_BUF := true

# SCALER
BOARD_USES_DEFAULT_CSC_HW_SCALER := true
BOARD_DEFAULT_CSC_HW_SCALER := 4
BOARD_USES_SCALER_M2M1SHOT := true
BOARD_HAS_SCALER_ALIGN_RESTRICTION := false

# LIBHWJPEG
TARGET_USES_UNIVERSAL_LIBHWJPEG := true

# IMS
# BOARD_USES_IMS := true

# Device Tree
BOARD_USES_DT := true

# PLATFORM LOG
TARGET_USES_LOGD := true

# ART
#ADDITIONAL_BUILD_PROPERTIES += dalvik.vm.image-dex2oat-filter=speed
#ADDITIONAL_BUILD_PROPERTIES += dalvik.vm.dex2oat-filter=speed

# FMP
#BOARD_USES_FMP_DM_CRYPT := true
#BOARD_USES_FMP_FSCRYPTO := true

ifeq ($(TARGET_PLATFORM_32BIT), true)
#S hack this macro should be changed depending on 32/64-bit kernel build
BOARD_USES_DECON_64BIT_ADDRESS := true
TARGET_USES_64_BIT_BINDER := true
endif
# Metadata
BOARD_USES_METADATA_PARTITION := true

# SELinux Platform Vendor policy for exynos
VENDOR_SEPOLICY := device/samsung/sepolicy/common \
			device/samsung/universal3830/sepolicy

ifeq (, $(findstring $(VENDOR_SEPOLICY), $(BOARD_VENDOR_SEPOLICY_DIRS)))
BOARD_VENDOR_SEPOLICY_DIRS += $(VENDOR_SEPOLICY)
endif

# SECCOMP policy
BOARD_SECCOMP_POLICY = device/samsung/universal3830/seccomp_policy

# SELinux Platform Private policy for exynos
SYSTEM_EXT_PRIVATE_SEPOLICY_DIRS := device/samsung/sepolicy/private

# SELinux Platform Public policy for exynos
SYSTEM_EXT_PUBLIC_SEPOLICY_DIRS := device/samsung/sepolicy/public

# NFC
# HACK R
#include device/samsung/universal3830/nfc/BoardConfig-nfc.mk

# include vendor/samsung_slsi/sscr_tools/BoardConfig.mk
#TARGET_NO_KERNEL := true
#CURL
# BOARD_USES_CURL := true

# VISION
# Exynos vision framework (EVF)
#TARGET_USES_EVF := true
# HW acceleration
#TARGET_USES_VPU_KERNEL := true
#TARGET_USES_SCORE_KERNEL := true
#TARGET_USES_CL_KERNEL := false

# SENSOR HUB
BOARD_USES_EXYNOS_SENSORS_DUMMY := true
#BOARD_USES_EXYNOS_SENSORS_HAL := true

# GNSS
BOARD_USES_EXYNOS_GNSS_HAL := true
ifeq ($(BOARD_USES_EXYNOS_GNSS_HAL),)
BOARD_USES_EXYNOS_GNSS_DUMMY := true
endif

TARGET_BOARD_KERNEL_HEADERS := hardware/samsung_slsi/exynos/kernel-4.19-headers/kernel-headers

ifeq ($(ENABLE_S_FEATURE),true)
# SYSTEM SDK
BOARD_SYSTEMSDK_VERSIONS := 31
TARGET_USES_MKE2FS := true
else ifeq ($(ENABLE_R_FEATURE),true)
# SYSTEM SDK
BOARD_SYSTEMSDK_VERSIONS := 30
TARGET_USES_MKE2FS := true
else
# SYSTEM SDK
BOARD_SYSTEMSDK_VERSIONS := 29
TARGET_USES_MKE2FS := true
endif

#VNDK
BOARD_PROPERTY_OVERRIDES_SPLIT_ENABLED := true
BOARD_VNDK_VERSION := current

#BOARD_LIBACRYL_DEFAULT_COMPOSITOR := fimg2d_3830
BOARD_LIBACRYL_DEFAULT_SCALER := mscl_3830
SOONG_CONFIG_NAMESPACES += libacryl
SOONG_CONFIG_libacryl += default_scaler
SOONG_CONFIG_libacryl_default_scaler := mscl_3830
#BOARD_LIBACRYL_DEFAULT_BLTER := fimg2d_9810_blter
#BOARD_LIBACRYL_G2D9810_HDR_PLUGIN := libacryl_plugin_slsi_hdr10

#Keymaster
BOARD_USES_KEYMASTER_VER4 := true

# WiFi related defines
BOARD_WPA_SUPPLICANT_DRIVER      := NL80211
BOARD_HOSTAPD_DRIVER             := NL80211
BOARD_WPA_SUPPLICANT_PRIVATE_LIB := lib_driver_cmd_slsi
BOARD_HOSTAPD_PRIVATE_LIB        := lib_driver_cmd_slsi
BOARD_HAS_SAMSUNG_WLAN           := true
WPA_SUPPLICANT_VERSION           := VER_0_8_X
BOARD_WLAN_DEVICE                := slsi
WIFI_DRIVER_MODULE_ARG           := ""
WLAN_VENDOR                      := 8
SCSC_WLAN_DEVICE_NAME            := nacho_s612
WIFI_HIDL_FEATURE_DUAL_INTERFACE := true

# Bluetooth related defines
BOARD_HAVE_BLUETOOTH := true
BOARD_HAVE_BLUETOOTH_SLSI := true
SOONG_CONFIG_NAMESPACES += libbt-vendor
SOONG_CONFIG_libbt-vendor := slsi
SOONG_CONFIG_libbt-vendor_slsi := true
BOARD_BLUETOOTH_BDROID_BUILDCFG_INCLUDE_DIR := hardware/samsung_slsi/libbt/include
BLUEDROID_HCI_VENDOR_STATIC_LINKING := false

# Enable BT/WIFI related code changes in Android source files
CONFIG_SAMSUNG_SCSC_WIFIBT       := true

# NS-IOT TEST SCRIPT. This should be REMOVED in ship build for security reason
# BOARD_USES_NSIOT_TESTSCRIPT := true

# To remove buildwarning :
# build/make/core/board_config.mk:180: warning: Building a 32-bit-app-only product on a 64-bit device.
# If this is intentional, set TARGET_SUPPORTS_64_BIT_APPS := false
ifeq ($(TARGET_PLATFORM_32BIT), true)
TARGET_SUPPORTS_64_BIT_APPS := false
else
TARGET_SUPPORTS_64_BIT_APPS := true
endif

# H/W align restriction of MM IPs
BOARD_EXYNOS_S10B_FORMAT_ALIGN := 64

# Do not build system image if WITH_ESSI specified as true
ifeq ($(WITH_ESSI),true)
PRODUCT_BUILD_SYSTEM_IMAGE := false
PRODUCT_BUILD_SYSTEM_OTHER_IMAGE := false
PRODUCT_BUILD_VENDOR_IMAGE := true
PRODUCT_BUILD_PRODUCT_IMAGE := false
PRODUCT_BUILD_PRODUCT_SERVICES_IMAGE := false
PRODUCT_BUILD_ODM_IMAGE := false
PRODUCT_BUILD_CACHE_IMAGE := false
PRODUCT_BUILD_RAMDISK_IMAGE := true
PRODUCT_BUILD_USERDATA_IMAGE := true
PRODUCT_BUILD_RECOVERY_IMAGE := true
PRODUCT_BUILD_BOOT_IMAGE := true

# Set VBMeta settings for chained partition
BOARD_AVB_SYSTEM_ALGORITHM := SHA256_RSA4096
BOARD_AVB_SYSTEM_KEY_PATH := device/samsung/universal9630/avbkey_rsa4096.pem
BOARD_AVB_SYSTEM_ROLLBACK_INDEX := 0
BOARD_AVB_SYSTEM_ROLLBACK_INDEX_LOCATION := 1

# Also, since we're going to skip building the system image, we also skip
# building the OTA package. We'll build this at a later step. We also don't
# need to build the OTA tools package (we'll use the one from the system build).
TARGET_SKIP_OTA_PACKAGE := true
TARGET_SKIP_OTATOOLS_PACKAGE := true
endif

BOARD_AVB_RECOVERY_KEY_PATH := device/samsung/universal3830/avbkey_rsa4096.pem
BOARD_AVB_RECOVERY_ALGORITHM := SHA256_RSA4096
BOARD_AVB_RECOVERY_ROLLBACK_INDEX := 0
BOARD_AVB_RECOVERY_ROLLBACK_INDEX_LOCATION := 0


# system as root device & dynamic partition support
# variable: PRODUCT_USE_DYNAMIC_PARTITIONS is readonly, we cannot set in BoardConfig,
# So, set below local variable to set it in device.mk
#BOARD_USE_DYNAMIC_PARTITIONS := true

# Enable system-as-root
# if BOARD_USE_DYNAMIC_PARTITIONS is true, system-as-root must be false
#PRODUCT_USE_SYSTEM_AS_ROOT_IMAGE := false

# For A/B device
AB_OTA_UPDATER := false

# Enable AVB2.0
BOARD_AVB_ENABLE := true
BOARD_BOOTIMAGE_PARTITION_SIZE := 0x06400000
ifeq ($(ENABLE_S_FEATURE),true)
BOARD_RECOVERYIMAGE_PARTITION_SIZE := 0x02D00000
else ifeq ($(ENABLE_R_FEATURE),true)
BOARD_RECOVERYIMAGE_PARTITION_SIZE := 0x02D00000
else
BOARD_RECOVERYIMAGE_PARTITION_SIZE := 0x02800000
endif
BOARD_DTBOIMG_PARTITION_SIZE := 0x00100000
BOARD_AVB_ALGORITHM := SHA256_RSA4096
BOARD_AVB_KEY_PATH := device/samsung/universal3830/avbkey_rsa4096.pem

# Enable Mobicore
BOARD_MOBICORE_ENABLE := true

# BOOT/VENDOR PATCH LEVEL FOR ROOT_OF_TRUST
BOOT_SECURITY_PATCH := $(PLATFORM_SECURITY_PATCH)
VENDOR_SECURITY_PATCH := $(PLATFORM_SECURITY_PATCH)

# Add config to allow use more buffer in Triple-buffering
CONFIG_USE_EXTRA_BUFFER := 1

# libExynosGraphicbuffer
SOONG_CONFIG_NAMESPACES += exynosgraphicbuffer
SOONG_CONFIG_exynosgraphicbuffer := \
	gralloc_version

ifeq ($(BOARD_USES_EXYNOS_GRALLOC_VERSION),3)
SOONG_CONFIG_exynosgraphicbuffer_gralloc_version := three
endif

ifeq ($(BOARD_USES_EXYNOS_GRALLOC_VERSION),4)
SOONG_CONFIG_exynosgraphicbuffer_gralloc_version := four
endif

ifeq ($(ENABLE_S_FEATURE),true)
# Gralloc4
SOONG_CONFIG_NAMESPACES += arm_gralloc
SOONG_CONFIG_arm_gralloc := \
	gralloc_arm_no_external_afbc \
	mali_gpu_support_afbc_basic \
	gralloc_init_afbc \
	gralloc_ion_sync_on_lock \
	gralloc_scaler_wfd

SOONG_CONFIG_arm_gralloc_gralloc_arm_no_external_afbc := true
SOONG_CONFIG_arm_gralloc_mali_gpu_support_afbc_basic := false
SOONG_CONFIG_arm_gralloc_gralloc_init_afbc := true
SOONG_CONFIG_arm_gralloc_gralloc_ion_sync_on_lock := $(BOARD_USES_GRALLOC_ION_SYNC)
SOONG_CONFIG_arm_gralloc_gralloc_scaler_wfd := $(BOARD_USES_GRALLOC_SCALER_WFD)
else ifeq ($(ENABLE_R_FEATURE),true)
# Gralloc4
SOONG_CONFIG_NAMESPACES += arm_gralloc
SOONG_CONFIG_arm_gralloc := \
	gralloc_arm_no_external_afbc \
	mali_gpu_support_afbc_basic \
	gralloc_init_afbc \
	gralloc_ion_sync_on_lock \
	gralloc_scaler_wfd

SOONG_CONFIG_arm_gralloc_gralloc_arm_no_external_afbc := true
SOONG_CONFIG_arm_gralloc_mali_gpu_support_afbc_basic := false
SOONG_CONFIG_arm_gralloc_gralloc_init_afbc := true
SOONG_CONFIG_arm_gralloc_gralloc_ion_sync_on_lock := $(BOARD_USES_GRALLOC_ION_SYNC)
SOONG_CONFIG_arm_gralloc_gralloc_scaler_wfd := $(BOARD_USES_GRALLOC_SCALER_WFD)
else
# Gralloc3
# Gralloc3 doesn't require SOONG_CONFIG_arm_gralloc flags
endif

# USB (USB gadgethal)
SOONG_CONFIG_NAMESPACES += usbgadgethal
SOONG_CONFIG_usbgadgethal:= exynos_product
SOONG_CONFIG_usbgadgethal_exynos_product := default

ifeq ($(MALI_ON_KEYSTONE),true)
# Allow deprecated BUILD_ module types required to build Mali UMD
BUILD_BROKEN_USES_BUILD_COPY_HEADERS := true
BUILD_BROKEN_USES_BUILD_HOST_EXECUTABLE := true
BUILD_BROKEN_USES_BUILD_HOST_SHARED_LIBRARY := true
BUILD_BROKEN_USES_BUILD_HOST_STATIC_LIBRARY := true
BUILD_BROKEN_MISSING_REQUIRED_MODULES := true
endif
