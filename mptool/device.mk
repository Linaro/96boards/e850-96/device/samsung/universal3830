#
# Copyright Samsung Electronics Co., LTD.
#
# This software is proprietary of Samsung Electronics.
# No part of this software, either material or conceptual may be copied or distributed, transmitted,
# transcribed, stored in a retrieval system or translated into any human or computer language in any form by any means,
# electronic, mechanical, manual or otherwise, or disclosed
# to third parties without the express written permission of Samsung Electronics.
#

#current_dir := $(dir $(TARGET_BOARD_INFO_FILE))
current_dir := $(dir $(lastword $(MAKEFILE_LIST)))
$(warning current mptool dir: $(current_dir))


PRODUCT_PACKAGES += \
        ringtone \
        libsftd_hal \
        libsftd_extmodem \
        tcmd_receive

PRODUCT_PACKAGES += \
        CustomCommand

PRODUCT_PACKAGES += \
        libwlbt

#PRODUCT_COPY_FILES += device/samsung/universal3830/gnss_binaries/release/lib_gnss.so:$(TARGET_COPY_OUT_VENDOR)/lib64/lib_gnss.so

ifeq ($(BOARD_USE_FACTORY_IMAGES), true)
PRODUCT_COPY_FILES += $(current_dir)conf/init.factory.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/init.factory.rc
PRODUCT_COPY_FILES += $(current_dir)conf/fstab.ufs:$(TARGET_COPY_OUT_VENDOR)/etc/init/fstab.factory
endif
PRODUCT_COPY_FILES += $(current_dir)asset/tinymix_mirror:$(TARGET_COPY_OUT_VENDOR)/bin/tinymix_mirror

