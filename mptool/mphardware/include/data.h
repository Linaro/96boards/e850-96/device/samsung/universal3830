#ifndef __ENGMODE_DATA_H__
#define __ENGMODE_DATA_H__

#ifdef __cplusplus
extern "C" {
#endif

#define MAX_FACTORY_CMD_PARAM_SIZE (1024*2)

typedef struct factory_cmd {
	char param[MAX_FACTORY_CMD_PARAM_SIZE];
	int param_len;
	int revert1;
	int revert2;
	void* revert3;
} SFCmdS;

#define RESPONSE_OK	"OK"
#define RESPONSE_ERROR	"ERROR"



#ifdef __cplusplus
}
#endif
#endif
