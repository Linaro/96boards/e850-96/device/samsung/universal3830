#
# Copyright (C) 2011 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

ifeq ($(ENABLE_S_FEATURE),true)
PRODUCT_SHIPPING_API_LEVEL := 31
else ifeq ($(ENABLE_R_FEATURE),true)
PRODUCT_SHIPPING_API_LEVEL := 30
else
PRODUCT_SHIPPING_API_LEVEL := 29
endif

include $(LOCAL_PATH)/BoardConfig.mk

ifneq ($(filter Q%, $(TARGET_BOARD_SUPPORT_FEATURE)),)
#BOARD_BOOT_HEADER_VERSION := 1
CONF_PATH_BASE := device/samsung/universal3830/conf
FSTAB_PATH_BASE := device/samsung/universal3830/conf
else ifneq ($(filter R%, $(TARGET_BOARD_SUPPORT_FEATURE)),)
#BOARD_BOOT_HEADER_VERSION := 3
CONF_PATH_BASE := device/samsung/universal3830/conf_api30
FSTAB_PATH_BASE := device/samsung/universal3830/conf_api30
else ifneq ($(filter S%, $(TARGET_BOARD_SUPPORT_FEATURE)),)
#BOARD_BOOT_HEADER_VERSION := 3
CONF_PATH_BASE := device/samsung/universal3830/conf_api31
FSTAB_PATH_BASE := device/samsung/universal3830/conf_api31/non_ab
endif


BOARD_PREBUILTS := device/samsung/$(TARGET_PRODUCT:full_%=%)-prebuilts
ifeq ($(ENABLE_S_FEATURE),true)
PREBUILD_PATH_BASE := $(BOARD_PREBUILTS)
INSTALLED_KERNEL_TARGET := $(BOARD_PREBUILTS)/kernel
BOARD_PREBUILT_DTBOIMAGE := $(BOARD_PREBUILTS)/dtbo.img
BOARD_PREBUILT_DTB := $(BOARD_PREBUILTS)/dtb.img
BOARD_PREBUILT_BOOTLOADER_IMG := $(BOARD_PREBUILTS)/bootloader.img
INSTALLED_DTBIMAGE_TARGET := $(BOARD_PREBUILT_DTB)

ifeq ($(wildcard $(BOARD_PREBUILTS)),)
PRODUCT_COPY_FILES += \
		$(INSTALLED_KERNEL_TARGET):$(PRODUCT_OUT)/kernel
else
PRODUCT_COPY_FILES += $(foreach image,\
		$(filter-out $(BOARD_PREBUILT_DTBOIMAGE) $(BOARD_PREBUILT_BOOTLOADER_IMG) $(BOARD_PREBUILT_DTB) $(BOARD_PREBUILT_KERNEL_MODULES) $(BOARD_PREBUILT_KERNEL_MODULES_SYMBOLS), $(wildcard $(BOARD_PREBUILTS)/*)),\
	$(image):$(PRODUCT_OUT)/$(notdir $(image)))
endif
else ifeq ($(ENABLE_R_FEATURE),true)
# Prebuilt images copy
INSTALLED_KERNEL_TARGET := $(BOARD_PREBUILTS)/kernel
BOARD_PREBUILT_DTBOIMAGE := $(BOARD_PREBUILTS)/dtbo.img
BOARD_PREBUILT_DTB := $(BOARD_PREBUILTS)/dtb.img
BOARD_PREBUILT_BOOTLOADER_IMG := $(BOARD_PREBUILTS)/bootloader.img
INSTALLED_DTBIMAGE_TARGET := $(BOARD_PREBUILT_DTB)

PRODUCT_COPY_FILES += $(foreach image, $(filter-out $(BOARD_PREBUILT_DTBOIMAGE) $(BOARD_PREBUILT_BOOTLOADER_IMG) $(BOARD_PREBUILT_DTB), $(wildcard $(BOARD_PREBUILTS)/*)), $(image):$(PRODUCT_OUT)/$(notdir $(image)))
else
# Prebuilt images copy
INSTALLED_KERNEL_TARGET := $(BOARD_PREBUILTS)/kernel
INSTALLED_DTBIMAGE_TARGET := $(BOARD_PREBUILTS)/dtb.img
BOARD_PREBUILT_DTBOIMAGE := $(BOARD_PREBUILTS)/dtbo.img
PRODUCT_COPY_FILES += $(foreach image,\
	$(filter-out $(BOARD_PREBUILT_DTBOIMAGE), $(wildcard $(BOARD_PREBUILTS)/*)),\
	$(image):$(PRODUCT_OUT)/$(notdir $(image)))
endif

# for tunnel
PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/android.software.ipsec_tunnels.xml:vendor/etc/permissions/android.software.ipsec_tunnels.xml

ifeq ($(BOARD_AVB_ENABLE), true)
# Enable Dynamic Partitions
PRODUCT_USE_DYNAMIC_PARTITIONS := true
BOARD_BUILD_SYSTEM_ROOT_IMAGE := false
endif

# System AS Root & Dynamic Partition support handle
ifeq ($(PRODUCT_USE_DYNAMIC_PARTITIONS), true)
# Dynamic Partitions options & config
BOARD_SUPER_PARTITION_SIZE := 3774873600
BOARD_SUPER_PARTITION_GROUPS := group_basic
BOARD_GROUP_BASIC_SIZE := 3770679296
BOARD_GROUP_BASIC_PARTITION_LIST := system vendor
endif

# For A/B device
ifeq ($(AB_OTA_UPDATER),true)
AB_OTA_PARTITIONS := \
  vbmeta \
  boot \
  system \
  dtbo \
  vendor
BOARD_USES_RECOVERY_AS_BOOT := true
PRODUCT_PACKAGES += \
  android.hardware.boot@1.0-impl \
  android.hardware.boot@1.0-service \
  bootctrl.$(TARGET_BOOTLOADER_BOARD_NAME) \
  bootctl \
  update_engine \
  update_verifier
endif

# Recovery mode
BOARD_INCLUDE_RECOVERY_DTBO := true
BOARD_INCLUDE_DTB_IN_BOOTIMG := true
BOARD_RAMDISK_OFFSET := 0
BOARD_KERNEL_TAGS_OFFSET := 0
BOARD_BOOTIMG_HEADER_VERSION := 2
ifeq ($(AB_OTA_UPDATER),true)
BOARD_MKBOOTIMG_ARGS := \
  --ramdisk_offset $(BOARD_RAMDISK_OFFSET) \
  --tags_offset $(BOARD_KERNEL_TAGS_OFFSET) \
  --header_version $(BOARD_BOOTIMG_HEADER_VERSION) \
  --recovery_dtbo $(BOARD_PREBUILT_DTBOIMAGE) \
  --dtb $(INSTALLED_DTBIMAGE_TARGET) \
  --dtb_offset 0
else
BOARD_MKBOOTIMG_ARGS := \
  --ramdisk_offset $(BOARD_RAMDISK_OFFSET) \
  --tags_offset $(BOARD_KERNEL_TAGS_OFFSET) \
  --header_version $(BOARD_BOOTIMG_HEADER_VERSION) \
  --dtb $(INSTALLED_DTBIMAGE_TARGET) \
  --dtb_offset 0
endif

ifeq ($(TARGET_BUILD_VARIANT),eng)
PRODUCT_SET_DEBUGFS_RESTRICTIONS := false
endif

# From system.property
PRODUCT_PROPERTY_OVERRIDES += \
    persist.demo.hdmirotationlock=false \
    dev.usbsetting.embedded=on \
    ro.vendor.config.release_version=20190306

ifeq ($(ENABLE_S_FEATURE),true)
PRODUCT_PROPERTY_OVERRIDES += ro.incremental.enable=yes
else ifeq ($(ENABLE_R_FEATURE),true)
PRODUCT_PROPERTY_OVERRIDES += ro.incremental.enable=yes
endif

ifeq ($(ENABLE_S_FEATURE),true)
# Device Manifest, Device Compatibility Matrix for Treble
DEVICE_MANIFEST_FILE := \
    device/samsung/universal3830/manifest_api31.xml
else ifeq ($(ENABLE_R_FEATURE),true)
# Device Manifest, Device Compatibility Matrix for Treble
DEVICE_MANIFEST_FILE := \
    device/samsung/universal3830/manifest_api30.xml
else
# Device Manifest, Device Compatibility Matrix for Treble
DEVICE_MANIFEST_FILE := \
    device/samsung/universal3830/manifest.xml
endif

ifeq ($(ENABLE_S_FEATURE),true)
DEVICE_FRAMEWORK_COMPATIBILITY_MATRIX_FILE := \
	device/samsung/universal3830/framework_compatibility_matrix_api31.xml
else ifeq ($(ENABLE_R_FEATURE),true)
DEVICE_FRAMEWORK_COMPATIBILITY_MATRIX_FILE := \
	device/samsung/universal3830/framework_compatibility_matrix_api30.xml
else
DEVICE_FRAMEWORK_COMPATIBILITY_MATRIX_FILE := \
	device/samsung/universal3830/framework_compatibility_matrix.xml
endif

ifeq ($(ENABLE_S_FEATURE),true)
DEVICE_MATRIX_FILE := \
    device/samsung/universal3830/compatibility_matrix_api31.xml
else ifeq ($(ENABLE_R_FEATURE),true)
DEVICE_MATRIX_FILE := \
    device/samsung/universal3830/compatibility_matrix_api30.xml
else
DEVICE_MATRIX_FILE := \
    device/samsung/universal3830/compatibility_matrix.xml
endif

# These are for the multi-storage mount.
ifeq ($(BOARD_USES_SDMMC_BOOT),true)
DEVICE_PACKAGE_OVERLAYS := \
	device/samsung/universal3830/overlay-sdboot
else
ifeq ($(BOARD_USES_UFS_BOOT),true)
DEVICE_PACKAGE_OVERLAYS := \
	device/samsung/universal3830/overlay-ufsboot
else
DEVICE_PACKAGE_OVERLAYS := \
	device/samsung/universal3830/overlay-emmcboot
endif
endif
DEVICE_PACKAGE_OVERLAYS += device/samsung/universal3830/overlay

# Init files
PRODUCT_COPY_FILES += \
	$(CONF_PATH_BASE)/init.exynos3830.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/init.$(TARGET_SOC).rc \
	$(CONF_PATH_BASE)/init.exynos3830.usb.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/init.$(TARGET_SOC).usb.rc \

PRODUCT_COPY_FILES += \
	$(CONF_PATH_BASE)/ueventd.exynos3830.rc:$(TARGET_COPY_OUT_VENDOR)/ueventd.rc \
	$(CONF_PATH_BASE)/init.recovery.exynos3830.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/init.recovery.$(TARGET_SOC).rc \
	$(CONF_PATH_BASE)/init.recovery.exynos3830.rc:root/init.recovery.$(TARGET_SOC).rc

# for off charging mode
PRODUCT_PACKAGES += \
	charger_res_images

ifeq ($(BOARD_USES_SDMMC_BOOT),true)
PRODUCT_COPY_FILES += \
	$(CONF_PATH_BASE)/fstab.exynos3830.sdboot:$(TARGET_COPY_OUT_VENDOR)/etc/fstab.$(TARGET_SOC)
else
ifeq ($(BOARD_USES_UFS_BOOT),true)
ifeq ($(BOARD_USE_FACTORY_IMAGES),true)
PRODUCT_COPY_FILES += \
        device/samsung/universal3830/mptool/conf/fstab.exynos3830:$(TARGET_COPY_OUT_VENDOR)/etc/fstab.$(TARGET_SOC)
else
PRODUCT_COPY_FILES += \
	$(CONF_PATH_BASE)/fstab.exynos3830:$(TARGET_COPY_OUT_VENDOR)/etc/fstab.$(TARGET_SOC)
endif #BOARD_USE_FACTORY_IMAGES
else
ifeq ($(PRODUCT_USE_DYNAMIC_PARTITIONS),true)
PRODUCT_COPY_FILES += \
	$(FSTAB_PATH_BASE)/fstab.exynos3830.emmc.dp:$(TARGET_COPY_OUT_VENDOR)/etc/fstab.$(TARGET_SOC) \
	$(FSTAB_PATH_BASE)/fstab.exynos3830.emmc.dp:$(TARGET_COPY_OUT_RAMDISK)/fstab.$(TARGET_SOC) \
	$(FSTAB_PATH_BASE)/fstab.exynos3830.emmc.dp:root/fstab.$(TARGET_SOC)
TARGET_RECOVERY_FSTAB := $(FSTAB_PATH_BASE)/fstab.exynos3830.emmc.dp
else
PRODUCT_COPY_FILES += \
	$(CONF_PATH_BASE)/fstab.exynos3830.emmc:$(TARGET_COPY_OUT_VENDOR)/etc/fstab.$(TARGET_SOC) \
	$(CONF_PATH_BASE)/fstab.exynos3830.emmc:$(TARGET_COPY_OUT_RAMDISK)/fstab.$(TARGET_SOC) \
	$(CONF_PATH_BASE)/fstab.exynos3830.emmc:root/fstab.$(TARGET_SOC)
TARGET_RECOVERY_FSTAB := $(CONF_PATH_BASE)/fstab.exynos3830.emmc
endif #BOARD_USE_DYNAMIC_PARTITIONS
endif #BOARD_USES_UFS_BOOT
endif #BOARD_USES_SDMMC_BOOT

# Support devtools
PRODUCT_PACKAGES += \
	Development

# Filesystem management tools
PRODUCT_PACKAGES += \
	e2fsck

# RPMB TA
PRODUCT_PACKAGES += \
	tlrpmb

# RPMB test application (only for eng build)
ifneq (,$(filter eng, $(TARGET_BUILD_VARIANT)))
PRODUCT_PACKAGES += \
	bRPMB
endif

# CBD (CP booting deamon)
CBD_USE_V2 := true

BOARD_USE_FM_RADIO := true
# FM Test App
PRODUCT_PACKAGES += \
	FMRadio \
	privapp-permissions-fmradio.xml

# boot animation files
PRODUCT_COPY_FILES += \
    device/samsung/universal3830/bootanim/bootanimation_part1.zip:system/media/bootanimation.zip \
    device/samsung/universal3830/bootanim/shutdownanimation.zip:system/media/shutdownanimation.zip

# color mode
PRODUCT_DEFAULT_PROPERTY_OVERRIDES += \
    persist.sys.sf.color_saturation=1.0

# dqe calib xml
#PRODUCT_COPY_FILES += \
    device/samsung/universal3830/dqe/hixmax-hix83112a.xml:$(TARGET_COPY_OUT_VENDOR)/etc/dqe/hixmax-hix83112a.xml \
    device/samsung/universal3830/dqe/novatek-nov36672a.xml:$(TARGET_COPY_OUT_VENDOR)/etc/dqe/novatek-nov36672a.xml \
    device/samsung/universal3830/dqe/samsung-s6e3fa0-vdo.xml:$(TARGET_COPY_OUT_VENDOR)/etc/dqe/samsung-s6e3fa0-vdo.xml \
    device/samsung/universal3830/dqe/default-lcd-vdo.xml:$(TARGET_COPY_OUT_VENDOR)/etc/dqe/default-lcd-vdo.xml \
    device/samsung/universal3830/dqe/calib_data.xml:$(TARGET_COPY_OUT_VENDOR)/etc/dqe/calib_data.xml

ifeq ($(BOARD_USES_EXYNOS_SENSORS_HAL),true)
# Enable support for chinook sensorhu
TARGET_USES_CHINOOK_SENSORHUB := false

#PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.hardware.sensor.gyroscope.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.sensor.gyroscope.xml\
    frameworks/native/data/etc/android.hardware.sensor.accelerometer.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.sensor.accelerometer.xml \
    device/samsung/universal3830/firmware/chub_bl.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/bl.unchecked.bin \
    device/samsung/universal3830/firmware/chub_nanohub_0.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/os.checked_0.bin \
    device/samsung/universal3830/firmware/chub_nanohub_1.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/os.checked_1.bin \
    device/samsung/universal3830/init.exynos.nanohub.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/init.exynos.sensorhub.rc \
    frameworks/native/data/etc/android.hardware.sensor.light.xml:system/etc/permissions/android.hardware.sensor.light.xml\
    frameworks/native/data/etc/android.hardware.sensor.barometer.xml:system/etc/permissions/android.hardware.sensor.barometer.xml \
    frameworks/native/data/etc/android.hardware.sensor.stepcounter.xml:system/etc/permissions/android.hardware.sensor.stepcounter.xml \
    frameworks/native/data/etc/android.hardware.sensor.stepdetector.xml:system/etc/permissions/android.hardware.sensor.stepdetector.xml \
    frameworks/native/data/etc/android.hardware.sensor.proximity.xml:system/etc/permissions/android.hardware.sensor.proximity.xml \
    frameworks/native/data/etc/android.hardware.sensor.compass.xml:system/etc/permissions/android.hardware.sensor.compass.xml \

# Sensor HAL
TARGET_USES_NANOHUB_SENSORHAL := true
NANOHUB_SENSORHAL_SENSORLIST := $(LOCAL_PATH)/sensorhal/sensorlist.cpp
NANOHUB_SENSORHAL_EXYNOS_CONTEXTHUB := true
NANOHUB_SENSORHAL_NAME_OVERRIDE := sensors.$(TARGET_BOARD_PLATFORM)

PRODUCT_PACKAGES += \
    $(NANOHUB_SENSORHAL_NAME_OVERRIDE) \
    context_hub.default \
    android.hardware.sensors@1.0-impl \
    android.hardware.sensors@1.0-service

# sensor utilities
PRODUCT_PACKAGES += \
    nanoapp_cmd

# sensor utilities (only for userdebug and eng builds)
ifneq (,$(filter userdebug eng, $(TARGET_BUILD_VARIANT)))
PRODUCT_PACKAGES += \
    nanotool \
    sensortest
endif
endif

ifeq ($(BOARD_USES_EXYNOS_SENSORS_DUMMY), true)
# Sensor HAL
PRODUCT_PACKAGES += \
	android.hardware.sensors@1.0-impl \
	android.hardware.sensors@1.0-service \
	sensors.$(TARGET_SOC)
endif

# USB HAL
PRODUCT_PACKAGES += \
	android.hardware.usb@1.1 \
	android.hardware.usb@1.1-service.$(TARGET_SOC)

# Lights HAL
#PRODUCT_PACKAGES += \
	lights.$(TARGET_SOC) \
	android.hardware.light@2.0-impl \
	android.hardware.light@2.0-service

# Fastboot HAL
PRODUCT_PACKAGES += \
       fastbootd

ifeq ($(ENABLE_S_FEATURE),true)
PRODUCT_SOONG_NAMESPACES += device/samsung/universal3830/power/aidl_api31
# Power AIDL
PRODUCT_PACKAGES += \
	android.hardware.power-service.exynos
else ifeq ($(ENABLE_R_FEATURE),true)
PRODUCT_SOONG_NAMESPACES += device/samsung/universal3830/power/aidl_api30
# Power AIDL
PRODUCT_PACKAGES += \
	android.hardware.power-service.exynos.3830
else
PRODUCT_SOONG_NAMESPACES += device/samsung/universal3830/power/hidl_api29
# Power HAL
PRODUCT_PACKAGES += \
	android.hardware.power@1.0-impl \
	android.hardware.power@1.0-service \
	power.$(TARGET_SOC)
endif

# configStore HAL
PRODUCT_PACKAGES += \
	android.hardware.configstore@1.0-service \
	android.hardware.configstore@1.0-impl \

# OFI HAL
# PRODUCT_PACKAGES += \
  vendor.samsung_slsi.hardware.ofi@1.0 \
  vendor.samsung_slsi.hardware.ofi@1.0-service
#  vendor.samsung_slsi.hardware.ofi@1.0-service-lazy

#PRODUCT_COPY_FILES += \
  device/samsung/universal3830/firmware/CC_DRAM_CODE_FLASH.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/CC_DRAM_CODE_FLASH.bin \
  device/samsung/universal3830/firmware/CC_DTCM_CODE_FLASH.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/CC_DTCM_CODE_FLASH.bin \
  device/samsung/universal3830/firmware/CC_ITCM_CODE_FLASH.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/CC_ITCM_CODE_FLASH.bin \
  device/samsung/universal3830/firmware/kernel_bin_enf.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/kernel_bin_enf.bin \
  device/samsung/universal3830/firmware/kernel_bin_inception.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/kernel_bin_inception.bin \
  device/samsung/universal3830/firmware/kernel_bin_mobile_vgg.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/kernel_bin_mobile_vgg.bin
# Thermal HAL
# HACK R
#PRODUCT_PACKAGES += \
	android.hardware.thermal@1.0-impl\
	android.hardware.thermal@1.0-service\
	thermal.$(TARGET_SOC)

ifeq ($(ENABLE_S_FEATURE),true)
#Health 1.0 HAL
PRODUCT_PACKAGES += \
	android.hardware.health@2.1-service \
	android.hardware.health@2.1-impl-exynos3830
else ifeq ($(ENABLE_R_FEATURE),true)
#Health 1.0 HAL
PRODUCT_PACKAGES += \
	android.hardware.health@2.1-service \
	android.hardware.health@2.1-impl-exynos3830
else
#Health 1.0 HAL
PRODUCT_PACKAGES += \
	android.hardware.health@2.0-service
endif

# Audio HALs
# Audio Configurations
USE_LEGACY_LOCAL_AUDIO_HAL := false
USE_XML_AUDIO_POLICY_CONF := 1

ifeq ($(ENABLE_S_FEATURE),true)
# Audio HAL Server & Default Implementations
PRODUCT_PACKAGES += \
	android.hardware.audio@2.0-service \
	android.hardware.audio@6.0-impl \
	android.hardware.audio.effect@6.0-impl \
	android.hardware.soundtrigger@2.3-impl
else ifeq ($(ENABLE_R_FEATURE),true)
# Audio HAL Server & Default Implementations
PRODUCT_PACKAGES += \
	android.hardware.audio@2.0-service \
	android.hardware.audio@6.0-impl \
	android.hardware.audio.effect@6.0-impl \
	android.hardware.soundtrigger@2.0-impl
else
# Audio HAL Server & Default Implementations
PRODUCT_PACKAGES += \
	android.hardware.audio@2.0-service \
	android.hardware.audio@5.0-impl \
	android.hardware.audio.effect@5.0-impl \
	android.hardware.soundtrigger@2.0-impl
endif

# AudioHAL libraries
PRODUCT_PACKAGES += \
	audio.primary.$(TARGET_SOC) \
	audio.usb.default \
	audio.r_submix.default \
	audio.bluetooth.default

# AudioHAL Configurations
PRODUCT_COPY_FILES += \
	device/samsung/universal3830/audio/config/usb_audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/usb_audio_policy_configuration.xml \
	frameworks/av/services/audiopolicy/config/a2dp_in_audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/a2dp_in_audio_policy_configuration.xml \
	device/samsung/universal3830/audio/config/bluetooth_audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/bluetooth_audio_policy_configuration.xml \
	frameworks/av/services/audiopolicy/config/r_submix_audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/r_submix_audio_policy_configuration.xml \
	frameworks/av/services/audiopolicy/config/audio_policy_volumes.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_policy_volumes.xml \
	frameworks/av/services/audiopolicy/config/default_volume_tables.xml:$(TARGET_COPY_OUT_VENDOR)/etc/default_volume_tables.xml \
	device/samsung/universal3830/audio/config/audio_policy_configuration.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_policy_configuration.xml \
	device/samsung/universal3830/audio/config/mixer_paths.xml:$(TARGET_COPY_OUT_VENDOR)/etc/mixer_paths.xml \
	device/samsung/universal3830/audio/config/audio_effects.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_effects.xml \
	device/samsung/universal3830/audio/config/audio_board_info.xml:$(TARGET_COPY_OUT_VENDOR)/etc/audio_board_info.xml

# Calliope firmware overwrite
PRODUCT_COPY_FILES += \
	device/samsung/universal3830/firmware/audio/calliope_dram.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/calliope_dram.bin \
	device/samsung/universal3830/firmware/audio/calliope_sram.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/calliope_sram.bin \
	device/samsung/universal3830/firmware/audio/calliope_dram_2.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/calliope_dram_2.bin \
	device/samsung/universal3830/firmware/audio/calliope_sram_2.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/calliope_sram_2.bin \
	device/samsung/universal3830/firmware/audio/abox_tplg.conf:$(TARGET_COPY_OUT_VENDOR)/firmware/abox_tplg.conf \
	device/samsung/universal3830/firmware/audio/abox_tplg.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/abox_tplg.bin \
	device/samsung/universal3830/firmware/audio/param_aprxse.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/param_aprxse.bin \
	device/samsung/universal3830/firmware/audio/param_aptxse.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/param_aptxse.bin \
	device/samsung/universal3830/firmware/audio/param_cprxse.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/param_cprxse.bin \
	device/samsung/universal3830/firmware/audio/param_cptxse.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/param_cptxse.bin \
	device/samsung/universal3830/firmware/audio/rxse.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/rxse.bin \
	device/samsung/universal3830/firmware/audio/txse.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/txse.bin \
	device/samsung/universal3830/firmware/audio/EXYNOS850_ERD_NACHO_slog.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/EXYNOS850_ERD_NACHO_slog.bin

# MIDI support native XML
PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/android.software.midi.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.software.midi.xml

# Enable AAudio MMAP/NOIRQ data path.
PRODUCT_PROPERTY_OVERRIDES += aaudio.mmap_policy=2
PRODUCT_PROPERTY_OVERRIDES += aaudio.mmap_exclusive_policy=2
PRODUCT_PROPERTY_OVERRIDES += aaudio.hw_burst_min_usec=2000

# A-Box Service Daemon
PRODUCT_PACKAGES += main_abox

ifneq ($(filter eng userdebug,$(TARGET_BUILD_VARIANT)),)
PRODUCT_PACKAGES += dssd
endif

# control_privapp_permissions
PRODUCT_PROPERTY_OVERRIDES += ro.control_privapp_permissions=enforce

# TinyTools for Audio
ifeq ($(TARGET_BUILD_VARIANT),eng)
PRODUCT_PACKAGES += \
    tinyplay \
    tinycap \
    tinymix \
    tinypcminfo \
    cplay
endif

# Libs
PRODUCT_PACKAGES += \
	com.android.future.usb.accessory

ifeq ($(BOARD_USES_EXYNOS_GRALLOC_VERSION),4)
PRODUCT_PACKAGES += \
	android.hardware.graphics.mapper@4.0-impl \
	android.hardware.graphics.allocator@4.0-service \
	android.hardware.graphics.allocator@4.0-impl
else
# for now include gralloc here. should come from hardware/samsung_slsi/exynos5
PRODUCT_PACKAGES += \
    android.hardware.graphics.mapper@2.0-impl-2.1 \
    android.hardware.graphics.allocator@2.0-service \
    android.hardware.graphics.allocator@2.0-impl \
    gralloc.$(TARGET_SOC)
endif

ifeq ($(ENABLE_S_FEATURE),true)
PRODUCT_PACKAGES += \
    libion_exynos \
    libion \
    android.hardware.memtrack-service.exynos
else
PRODUCT_PACKAGES += \
    memtrack.$(TARGET_BOOTLOADER_BOARD_NAME)\
    android.hardware.memtrack@1.0-impl \
    android.hardware.memtrack@1.0-service \
    libion
endif

PRODUCT_PACKAGES += \
    libhwjpeg

# Video Editor
PRODUCT_PACKAGES += \
	VideoEditorGoogle

# WideVine modules -- For Nacho S launching, RS MR and QRS MR
ifeq ($(ENABLE_S_FEATURE),true)
PRODUCT_PACKAGES += \
	android.hardware.drm@1.4-service.clearkey \
        android.hardware.drm@1.4-service.widevine
else ifeq ($(ENABLE_R_FEATURE),true)
PRODUCT_PACKAGES += \
	android.hardware.drm@1.4-service.clearkey \
        android.hardware.drm@1.4-service.widevine
else
PRODUCT_PACKAGES += \
	android.hardware.drm@1.0-impl \
        android.hardware.drm@1.0-service \
	android.hardware.drm@1.4-service.clearkey \
	android.hardware.drm@1.4-service.widevine
endif

# SecureDRM modules
PRODUCT_PACKAGES += \
	tlwvdrm \
	tlsecdrm \
	liboemcrypto_modular

# HDCP module for WFD
PRODUCT_PACKAGES += \
	libstagefright_hdcp

ifeq ($(BOARD_MOBICORE_ENABLE), true)
# MobiCore namespace
PRODUCT_SOONG_NAMESPACES += hardware/samsung_slsi/exynos/tee/kinibi500

# MobiCore setup
PRODUCT_PACKAGES += \
	libMcClient \
	libMcRegistry \
	libgdmcprov \
	mcDriverDaemon \
	vendor.trustonic.tee@1.1-service \
	RootPA \
	TeeService \
	libTeeClient \
	libteeservice_client.trustonic \
	tee_tlcm \
	tee_whitelist \
	public.libraries-trustonic.txt
endif

PRODUCT_SOONG_NAMESPACES += hardware/samsung_slsi/exynos/tee/TlcTeeKeymaster4

# SPI driver
#PRODUCT_PACKAGES += \
	 tlspidrv

# Camera HAL
#PRODUCT_PACKAGES += \
    android.hardware.camera.provider@2.4-impl \
    android.hardware.camera.provider@2.4-service \
    camera.$(TARGET_SOC)

# Copy FIMC_IS DDK Libraries
#PRODUCT_COPY_FILES += \
    device/samsung/universal3830/firmware/camera/fimc_is_lib.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/fimc_is_lib.bin \
    device/samsung/universal3830/firmware/camera/fimc_is_rta.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/fimc_is_rta.bin \
    device/samsung/universal3830/firmware/camera/setfile_2p7sq.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_2p7sq.bin \
    device/samsung/universal3830/firmware/camera/setfile_2t7sx.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_2t7sx.bin \
    device/samsung/universal3830/firmware/camera/setfile_6b2.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_6b2.bin \
    device/samsung/universal3830/firmware/camera/setfile_2x5sp.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_2x5sp.bin\
    device/samsung/universal3830/firmware/camera/setfile_5e9.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_5e9.bin \
    device/samsung/universal3830/firmware/camera/setfile_gm1sp.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/setfile_gm1sp.bin

# Copy OIS Libraries
#PRODUCT_COPY_FILES += \
    device/samsung/universal3830/firmware/camera/bu24218_cal_data_default.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/bu24218_cal_data_default.bin \
    device/samsung/universal3830/firmware/camera/bu24218_Rev1.5_S_data1.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/bu24218_Rev1.5_S_data1.bin \
    device/samsung/universal3830/firmware/camera/bu24218_Rev1.5_S_data2.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/bu24218_Rev1.5_S_data2.bin

# Copy VIPx Firmware
#PRODUCT_COPY_FILES += \
    device/samsung/universal3830/firmware/camera/vipx/CC_DRAM_CODE_FLASH_HIFI.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/CC_DRAM_CODE_FLASH_HIFI.bin \
    device/samsung/universal3830/firmware/camera/vipx/CC_DTCM_CODE_FLASH_HIFI.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/CC_DTCM_CODE_FLASH_HIFI.bin \
    device/samsung/universal3830/firmware/camera/vipx/CC_ITCM_CODE_FLASH_HIFI.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/CC_ITCM_CODE_FLASH_HIFI.bin

# Copy Camera HFD Setfiles
#PRODUCT_COPY_FILES += \
    device/samsung/universal3830/firmware/camera/libhfd/default_configuration.hfd.cfg.json:$(TARGET_COPY_OUT_VENDOR)/firmware/default_configuration.hfd.cfg.json \
    device/samsung/universal3830/firmware/camera/libhfd/pp_cfg.json:$(TARGET_COPY_OUT_VENDOR)/firmware/pp_cfg.json \
    device/samsung/universal3830/firmware/camera/libhfd/tracker_cfg.json:$(TARGET_COPY_OUT_VENDOR)/firmware/tracker_cfg.json \
    device/samsung/universal3830/firmware/camera/libhfd/WithLightFixNoBN.SDNNmodel:$(TARGET_COPY_OUT_VENDOR)/firmware/WithLightFixNoBN.SDNNmodel

PRODUCT_COPY_FILES += \
	device/samsung/universal3830/handheld_core_hardware.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/handheld_core_hardware.xml \
	frameworks/native/data/etc/android.hardware.camera.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.camera.xml \
	frameworks/native/data/etc/android.hardware.camera.front.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.camera.front.xml \
	frameworks/native/data/etc/android.hardware.camera.flash-autofocus.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.camera.flash-autofocus.xml \
	frameworks/native/data/etc/android.hardware.camera.full.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.camera.full.xml \
	frameworks/native/data/etc/android.hardware.camera.raw.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.camera.raw.xml \
	frameworks/native/data/etc/android.hardware.wifi.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.wifi.xml \
	frameworks/native/data/etc/android.hardware.wifi.direct.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.wifi.direct.xml \
	frameworks/native/data/etc/android.hardware.touchscreen.multitouch.jazzhand.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.touchscreen.multitouch.jazzhand.xml \
	frameworks/native/data/etc/android.hardware.usb.host.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.usb.host.xml \
	frameworks/native/data/etc/android.hardware.usb.accessory.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.usb.accessory.xml \
	frameworks/native/data/etc/android.hardware.audio.low_latency.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.audio.low_latency.xml

# WLAN configuration
# device specific wpa_supplicant.conf
PRODUCT_COPY_FILES += \
		       device/samsung/universal3830/wifi/p2p_supplicant.conf:$(TARGET_COPY_OUT_VENDOR)/etc/wifi/p2p_supplicant.conf \
		       device/samsung/universal3830/wpa_supplicant.conf:$(TARGET_COPY_OUT_VENDOR)/etc/wifi/wpa_supplicant.conf

# Bluetooth libbt namespace
PRODUCT_SOONG_NAMESPACES += hardware/samsung_slsi/libbt

# Bluetooth configuration
PRODUCT_COPY_FILES += \
       frameworks/native/data/etc/android.hardware.bluetooth.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.bluetooth.xml \
       hardware/samsung_slsi/libbt/conf/bt_did.conf:$(TARGET_COPY_OUT_VENDOR)/etc/bluetooth/bt_did.conf \
       frameworks/native/data/etc/android.hardware.bluetooth_le.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.bluetooth_le.xml

PRODUCT_PROPERTY_OVERRIDES += \
        wifi.interface=wlan0

# Packages needed for WLAN
# Note android.hardware.wifi@1.0-service is used by HAL interface 1.2
PRODUCT_PACKAGES += \
    android.hardware.wifi@1.0-service \
    android.hardware.wifi.supplicant@1.1-service \
    android.hardware.wifi.offload@1.0-service \
    dhcpcd.conf \
    hostapd \
    wificond \
    wifilog \
    wpa_supplicant \
    wpa_supplicant.conf \
    wpa_cli \
    hostapd_cli

PRODUCT_PACKAGES += \
    libwifi-hal \
    libwifi-system \
    libwifikeystorehal \
    android.hardware.wifi@1.3 \
    android.hardware.wifi.supplicant@1.2

#PRODUCT_PROPERTY_OVERRIDES += wifi.supplicant_scan_interval=15

# Bluetooth HAL
PRODUCT_PACKAGES += \
 android.hardware.bluetooth@1.0-service\
    android.hardware.bluetooth@1.0-impl \
    libbt-vendor

# Override for providing bluetooth adress fallback and turning audio_hal (new in Android Q off)
PRODUCT_DEFAULT_PROPERTY_OVERRIDES += \
    ro.bt.bdaddr_path=/sys/module/scsc_bt/parameters/bluetooth_address_fallback

PRODUCT_PACKAGES += \
    android.hardware.bluetooth.audio-impl

# Set supported Bluetooth profiles to enabled
PRODUCT_PROPERTY_OVERRIDES += \
    bluetooth.profile.a2dp.source.enabled=true \
    bluetooth.profile.avrcp.target.enabled=true \
    bluetooth.profile.gatt.enabled=true \
    bluetooth.profile.hfp.ag.enabled=true \
    bluetooth.profile.hid.device.enabled=true \
    bluetooth.profile.hid.host.enabled=true \
    bluetooth.profile.map.server.enabled=true \
    bluetooth.profile.opp.enabled=true \
    bluetooth.profile.pan.nap.enabled=true \
    bluetooth.profile.pan.panu.enabled=true \
    bluetooth.profile.pbap.server.enabled=true

PRODUCT_PROPERTY_OVERRIDES += \
	ro.opengles.version=196610 \
	ro.sf.lcd_density=320 \
	debug.slsi_platform=1 \
	debug.hwc.winupdate=1 \
	debug.sf.disable_backpressure=1

# adoptable storage
PRODUCT_PROPERTY_OVERRIDES += ro.crypto.volume.filenames_mode=aes-256-cts
#PRODUCT_PROPERTY_OVERRIDES += ro.crypto.allow_encrypt_override=true

# LMK
PRODUCT_PROPERTY_OVERRIDES += \
	ro.lmk.use_minfree_levels=true

ifeq ($(ENABLE_S_FEATURE),true)
PRODUCT_PACKAGES += vndservicemanager
else ifeq ($(ENABLE_R_FEATURE),true)
PRODUCT_PACKAGES += vndservicemanager
endif

# Epic HIDL
SOONG_CONFIG_NAMESPACES += epic
SOONG_CONFIG_epic := vendor_hint
SOONG_CONFIG_epic_vendor_hint := true

PRODUCT_PACKAGES += \
        vendor.samsung_slsi.hardware.epic@1.0-impl \
        vendor.samsung_slsi.hardware.epic@1.0-service


# hw composer HAL
PRODUCT_PACKAGES += \
	hwcomposer.$(TARGET_BOOTLOADER_BOARD_NAME) \
    android.hardware.graphics.composer@2.2-impl \
    android.hardware.graphics.composer@2.2-service \
    vendor.samsung_slsi.hardware.ExynosHWCServiceTW@1.0-service

# hw composer property : Properties related to hwc will be defined in hwcomposer_property.mk
$(call inherit-product-if-exists, hardware/samsung_slsi/graphics/base/hwcomposer_property.mk)

# set the dss enable status setup
ifeq ($(BOARD_USES_EXYNOS5_DSS_FEATURE), true)
#PRODUCT_PROPERTY_OVERRIDES += \
        ro.exynos.dss=1
endif

# set the dss enable status setup
ifeq ($(BOARD_USES_EXYNOS_AFBC_FEATURE), true)
#PRODUCT_PROPERTY_OVERRIDES += \
        ro.vendor.ddk.set.afbc=1
endif

PRODUCT_CHARACTERISTICS := phone

PRODUCT_AAPT_CONFIG := normal hdpi xhdpi xxhdpi
PRODUCT_AAPT_PREF_CONFIG := xxhdpi


####################################
## VIDEO
####################################
# MFC firmware
PRODUCT_COPY_FILES += \
	device/samsung/universal3830/firmware/mfc_fw_v10.21.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/mfc_fw.bin

# 1. Codec 2.0
ifeq ($(BOARD_USE_DEFAULT_SERVICE), true)
# dummy service
PRODUCT_SOONG_NAMESPACES += hardware/samsung_slsi/exynos/c2service

DEVICE_MANIFEST_FILE += \
	device/samsung/universal3830/manifest_media_c2_default.xml

PRODUCT_COPY_FILES += \
	device/samsung/universal3830/media_codecs_performance_c2.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs_performance_c2.xml

PRODUCT_PACKAGES += \
    samsung.hardware.media.c2@1.1-default-service
endif

# 2. OpenMAX IL
# OpenMAX IL configuration files
PRODUCT_COPY_FILES += \
	device/samsung/universal3830/media_codecs_performance.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs_performance.xml \
	device/samsung/universal3830/media_codecs.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs.xml

PRODUCT_COPY_FILES += \
	device/samsung/universal3830/seccomp_policy/mediacodec-seccomp.policy:$(TARGET_COPY_OUT_VENDOR)/etc/seccomp_policy/mediacodec.policy

PRODUCT_PACKAGES += \
   libstagefrighthw \
   libExynosOMX_Core \
   libExynosOMX_Resourcemanager \
   libOMX.Exynos.MPEG4.Decoder \
   libOMX.Exynos.AVC.Decoder \
   libOMX.Exynos.WMV.Decoder \
   libOMX.Exynos.VP8.Decoder \
   libOMX.Exynos.HEVC.Decoder \
   libOMX.Exynos.MPEG4.Encoder \
   libOMX.Exynos.AVC.Encoder \
   libOMX.Exynos.VP8.Encoder \
   libOMX.Exynos.HEVC.Encoder \
####################################

# Camera
PRODUCT_COPY_FILES += \
	device/samsung/universal3830/media_profiles.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_profiles_V1_0.xml

# Telephony
PRODUCT_COPY_FILES += \
	frameworks/av/media/libstagefright/data/media_codecs_google_telephony.xml:$(TARGET_COPY_OUT_VENDOR)/etc/media_codecs_google_telephony.xml

# setup dalvik vm configs.
$(call inherit-product, frameworks/native/build/phone-xhdpi-2048-dalvik-heap.mk)

PRODUCT_TAGS += dalvik.gc.type-precise

# Exynos OpenVX framework
# PRODUCT_PACKAGES += \
		libexynosvision

ifeq ($(TARGET_USES_CL_KERNEL),true)
PRODUCT_PACKAGES += \
       libopenvx-opencl
endif

#GPS
PRODUCT_PACKAGES += \
    android.hardware.gnss@2.1-impl \
	vendor.samsung.hardware.gnss@2.0-impl \
    vendor.samsung.hardware.gnss@2.0-service

#PRODUCT_PROPERTY_OVERRIDES += \
    persist.vendor.gnsslog.maxfilesize=256 \
    persist.vendor.gnsslog.status=0 \
    exynos.gnss.path.log=/data/vendor/gps/

#Gatekeeper
PRODUCT_PACKAGES += \
	android.hardware.gatekeeper@1.0-impl \
	android.hardware.gatekeeper@1.0-service \
	gatekeeper.$(TARGET_SOC)

# CryptoManger
PRODUCT_PACKAGES += \
	tlcmdrv \

# CryptoManager test app for eng build
ifneq (,$(filter eng, $(TARGET_BUILD_VARIANT)))
PRODUCT_PACKAGES += \
	tlcmtest \
	cm_test
endif

# Keymaster
PRODUCT_PACKAGES += \
	android.hardware.keymaster@4.0-impl \
	android.hardware.keymaster@4.0_tee-service \
	tlkeymasterM \
	keymaster_drv

# ArmNN driver
# PRODUCT_PACKAGES += \
  android.hardware.neuralnetworks@1.1-service-armnn

PRODUCT_PROPERTY_OVERRIDES += \
	ro.frp.pst=/dev/block/platform/12100000.dwmmc0/by-name/frp

# RenderScript HAL
PRODUCT_PACKAGES += \
	android.hardware.renderscript@1.0-impl

# FEATURE_OPENGLES_EXTENSION_PACK support string config file
PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/android.hardware.opengles.aep.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.opengles.aep.xml

# vulkan version information
PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/android.hardware.vulkan.compute-0.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.vulkan.compute.xml \
	frameworks/native/data/etc/android.hardware.vulkan.level-1.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.vulkan.level.xml \
	frameworks/native/data/etc/android.hardware.vulkan.version-1_1.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.hardware.vulkan.version.xml

ifeq ($(ENABLE_S_FEATURE),true)
# OpenGL ES and Vulkan DEQP version information
PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/android.software.opengles.deqp.level-2021-03-01.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.software.opengles.deqp.level.xml \
	frameworks/native/data/etc/android.software.vulkan.deqp.level-2021-03-01.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.software.vulkan.deqp.level.xml
else ifeq ($(ENABLE_R_FEATURE),true)
PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/android.software.vulkan.deqp.level-2020-03-01.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.software.vulkan.deqp.level.xml
else
PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/android.software.vulkan.deqp.level-2019-03-01.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/android.software.vulkan.deqp.level.xml
endif

# AVB2.0, to tell PackageManager that the system supports Verified Boot(PackageManager.FEATURE_VERIFIED_BOOT)
ifeq ($(BOARD_AVB_ENABLE), true)
PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/android.software.verified_boot.xml:system/etc/permissions/android.software.verified_boot.xml
endif

#VNDK
PRODUCT_PACKAGES += \
	vndk-libs

PRODUCT_ENFORCE_RRO_TARGETS := \
	framework-res

ifneq ($(MALI_ON_KEYSTONE),true)
PRODUCT_PROPERTY_OVERRIDES += \
	ro.hardware.egl=mali \
	ro.hardware.vulkan=mali
ifeq ($(ENABLE_S_FEATURE),true)
PRODUCT_SOONG_NAMESPACES += vendor/samsung_slsi/exynos3830/libs/3830_gralloc4
#VENDOR
PRODUCT_PACKAGES += \
	libGLES_mali \
	libOpenCL_prebuilt \
	vulkan.mali_prebuilt
else ifeq ($(ENABLE_R_FEATURE),true)
PRODUCT_SOONG_NAMESPACES += vendor/samsung_slsi/exynos3830/libs/3830_gralloc4
#VENDOR
PRODUCT_PACKAGES += \
	libGLES_mali \
	libOpenCL_prebuilt \
	vulkan.mali_prebuilt
else
PRODUCT_SOONG_NAMESPACES += vendor/samsung_slsi/exynos3830/libs/3830_gralloc3
#VENDOR
PRODUCT_PACKAGES += \
	libGLES_mali \
	libOpenCL_prebuilt \
	vulkan.mali_prebuilt
endif
endif

# whitelist for public libraries such as libOpenCL.so
PRODUCT_PACKAGES += whitelist

PRODUCT_PACKAGES += \
	mfc_fw.bin

ifeq ($(ENABLE_S_FEATURE),true)
$(call inherit-product, $(SRC_TARGET_DIR)/product/emulated_storage.mk)
else ifeq ($(ENABLE_R_FEATURE),true)
$(call inherit-product, $(SRC_TARGET_DIR)/product/emulated_storage.mk)
endif
ifneq ($(TARGET_PLATFORM_32BIT), true)
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
endif
$(call inherit-product, $(SRC_TARGET_DIR)/product/aosp_base.mk)

## telephony.mk, relevant packages and properties will be defined in device-vendor.mk
#$(call inherit-product, vendor/samsung_slsi/telephony/common/device-vendor.mk)
##$(call inherit-product, hardware/samsung_slsi/exynos5/exynos5.mk)
$(call inherit-product-if-exists, hardware/samsung_slsi/exynos3830/exynos3830.mk)
$(call inherit-product-if-exists, vendor/samsung_slsi/common/exynos-vendor_v2.mk)
##$(call inherit-product, device/samsung/universal3830/telephony_binaries/telephony_binaries.mk)
## $(call inherit-product-if-exists, vendor/samsung_slsi/exynos/camera/hal3/camera.mk)
$(call inherit-product, device/samsung/universal3830/gnss_binaries/gnss_binaries.mk)
#$(call inherit-product-if-exists, vendor/samsung_slsi/telephony/exynos-ril/telephony_binaries.mk)

# Installs gsi keys into ramdisk, to boot a GSI with verified boot.
#$(call inherit-product, $(SRC_TARGET_DIR)/product/gsi_keys.mk)

## MAKE mx file for WLBT ##
ifneq ($(findstring mx, $(wildcard vendor/samsung_slsi/mx140/firmware/mx)),)
PRODUCT_COPY_FILES += vendor/samsung_slsi/mx140/firmware/mx:vendor/etc/wifi/mx
endif

# top level makefile to call all necessary SCSC Wifi-BT makefiles
$(call inherit-product-if-exists, device/samsung/universal3830/scsc_wlbt.mk)

# PRODUCT_PACKAGES for vendor/samsung_slsi/scsc_tools/wifi-bt/
$(call inherit-product-if-exists, vendor/samsung_slsi/scsc_tools/wifi-bt/wifi-bt.mk)

# PRODUCT_PACKAGES for vendor/samsung_slsi/scsc_tools/test_engine
$(call inherit-product-if-exists, vendor/samsung_slsi/scsc_tools/test_engine/test_engine.mk)

# PRODUCT_PACKAGES for vendor/samsung_slsi/scsc_tools/kic
$(call inherit-product-if-exists, vendor/samsung_slsi/scsc_tools/kic/kic.mk)

ro.arch=exynos3830

## WLBT Logging ##
$(call inherit-product, vendor/samsung_slsi/scsc_tools/wlbt/device-vendor.mk)

# IMS feature enable.
#PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.hardware.telephony.ims.xml:system/etc/permissions/android.hardware.telephony.ims.xml
# IMS WiFi Calling #
#PRODUCT_COPY_FILES += \
    packages/apps/ShannonIms/bin/PacketRouter/wfc-pkt-router:/system/bin/wfc-pkt-router \
    packages/apps/ShannonIms/bin/StrongSwan/charon:$(TARGET_COPY_OUT_VENDOR)/bin/charon \
    packages/apps/ShannonIms/bin/StrongSwan/lib/libcharon.so:/vendor/lib/libcharon.so \
    packages/apps/ShannonIms/bin/StrongSwan/lib/libhydra.so:/vendor/lib/libhydra.so \
    packages/apps/ShannonIms/bin/StrongSwan/lib/libstrongswan.so:/vendor/lib/libstrongswan.so \
    packages/apps/ShannonIms/bin/StrongSwan/lib/libstdc++.so:/vendor/lib/libstdc++.so \
    packages/apps/ShannonIms/bin/StrongSwan/conf/pcscf_routes.sh:/system/etc/pcscf_routes.sh \
    packages/apps/ShannonIms/bin/StrongSwan/conf/strongswan.conf:/system/etc/strongswan.conf \
    packages/apps/ShannonIms/bin/StrongSwan/conf/ipsec.conf:/system/etc/ipsec.conf \
    packages/apps/ShannonIms/bin/StrongSwan/conf/ipsec.secrets:/system/etc/ipsec.secrets \
    packages/apps/ShannonIms/bin/StrongSwan/conf/strongswan.d/charon.conf:/system/etc/strongswan.d/charon.conf \
    packages/apps/ShannonIms/bin/StrongSwan/conf/strongswan.d/charon-logging.conf:/system/etc/strongswan.d/charon-logging.conf \
    packages/apps/ShannonIms/bin/StrongSwan/conf/strongswan.d/pki.conf:/system/etc/strongswan.d/pki.conf \
    packages/apps/ShannonIms/bin/StrongSwan/conf/strongswan.d/scepclient.conf:/system/etc/strongswan.d/scepclient.conf \
    packages/apps/ShannonIms/bin/StrongSwan/conf/strongswan.d/starter.conf:/system/etc/strongswan.d/starter.conf \
    packages/apps/ShannonIms/bin/StrongSwan/conf/strongswan.d/charon/aes.conf:/system/etc/strongswan.d/charon/aes.conf \
    packages/apps/ShannonIms/bin/StrongSwan/conf/strongswan.d/charon/attr.conf:/system/etc/strongswan.d/charon/attr.conf \
    packages/apps/ShannonIms/bin/StrongSwan/conf/strongswan.d/charon/cmac.conf:/system/etc/strongswan.d/charon/cmac.conf \
    packages/apps/ShannonIms/bin/StrongSwan/conf/strongswan.d/charon/constraints.conf:/system/etc/strongswan.d/charon/constraints.conf \
    packages/apps/ShannonIms/bin/StrongSwan/conf/strongswan.d/charon/des.conf:/system/etc/strongswan.d/charon/des.conf \
    packages/apps/ShannonIms/bin/StrongSwan/conf/strongswan.d/charon/dnskey.conf:/system/etc/strongswan.d/charon/dnskey.conf \
    packages/apps/ShannonIms/bin/StrongSwan/conf/strongswan.d/charon/eap-aka.conf:/system/etc/strongswan.d/charon/eap-aka.conf \
    packages/apps/ShannonIms/bin/StrongSwan/conf/strongswan.d/charon/eap-aka-3gpp2.conf:/system/etc/strongswan.d/charon/eap-aka-3gpp2.conf \
    packages/apps/ShannonIms/bin/StrongSwan/conf/strongswan.d/charon/eap-sim-pcsc.conf:/system/etc/strongswan.d/charon/eap-sim-pcsc.conf \
    packages/apps/ShannonIms/bin/StrongSwan/conf/strongswan.d/charon/fips-prf.conf:/system/etc/strongswan.d/charon/fips-prf.conf \
    packages/apps/ShannonIms/bin/StrongSwan/conf/strongswan.d/charon/gmp.conf:/system/etc/strongswan.d/charon/gmp.conf \
    packages/apps/ShannonIms/bin/StrongSwan/conf/strongswan.d/charon/hmac.conf:/system/etc/strongswan.d/charon/hmac.conf \
    packages/apps/ShannonIms/bin/StrongSwan/conf/strongswan.d/charon/kernel-netlink.conf:/system/etc/strongswan.d/charon/kernel-netlink.conf \
    packages/apps/ShannonIms/bin/StrongSwan/conf/strongswan.d/charon/md5.conf:/system/etc/strongswan.d/charon/md5.conf \
    packages/apps/ShannonIms/bin/StrongSwan/conf/strongswan.d/charon/nonce.conf:/system/etc/strongswan.d/charon/nonce.conf \
    packages/apps/ShannonIms/bin/StrongSwan/conf/strongswan.d/charon/openssl.conf:/system/etc/strongswan.d/charon/openssl.conf \
    packages/apps/ShannonIms/bin/StrongSwan/conf/strongswan.d/charon/pem.conf:/system/etc/strongswan.d/charon/pem.conf \
    packages/apps/ShannonIms/bin/StrongSwan/conf/strongswan.d/charon/pgp.conf:/system/etc/strongswan.d/charon/pgp.conf \
    packages/apps/ShannonIms/bin/StrongSwan/conf/strongswan.d/charon/pkcs1.conf:/system/etc/strongswan.d/charon/pkcs1.conf \
    packages/apps/ShannonIms/bin/StrongSwan/conf/strongswan.d/charon/pkcs7.conf:/system/etc/strongswan.d/charon/pkcs7.conf \
    packages/apps/ShannonIms/bin/StrongSwan/conf/strongswan.d/charon/pkcs8.conf:/system/etc/strongswan.d/charon/pkcs8.conf \
    packages/apps/ShannonIms/bin/StrongSwan/conf/strongswan.d/charon/pkcs12.conf:/system/etc/strongswan.d/charon/pkcs12.conf \
    packages/apps/ShannonIms/bin/StrongSwan/conf/strongswan.d/charon/pubkey.conf:/system/etc/strongswan.d/charon/pubkey.conf \
    packages/apps/ShannonIms/bin/StrongSwan/conf/strongswan.d/charon/random.conf:/system/etc/strongswan.d/charon/random.conf \
    packages/apps/ShannonIms/bin/StrongSwan/conf/strongswan.d/charon/rc2.conf:/system/etc/strongswan.d/charon/rc2.conf \
    packages/apps/ShannonIms/bin/StrongSwan/conf/strongswan.d/charon/resolve.conf:/system/etc/strongswan.d/charon/resolve.conf \
    packages/apps/ShannonIms/bin/StrongSwan/conf/strongswan.d/charon/revocation.conf:/system/etc/strongswan.d/charon/revocation.conf \
    packages/apps/ShannonIms/bin/StrongSwan/conf/strongswan.d/charon/sha1.conf:/system/etc/strongswan.d/charon/sha1.conf \
    packages/apps/ShannonIms/bin/StrongSwan/conf/strongswan.d/charon/sha2.conf:/system/etc/strongswan.d/charon/sha2.conf \
    packages/apps/ShannonIms/bin/StrongSwan/conf/strongswan.d/charon/socket-default.conf:/system/etc/strongswan.d/charon/socket-default.conf \
    packages/apps/ShannonIms/bin/StrongSwan/conf/strongswan.d/charon/sshkey.conf:/system/etc/strongswan.d/charon/sshkey.conf \
    packages/apps/ShannonIms/bin/StrongSwan/conf/strongswan.d/charon/stroke.conf:/system/etc/strongswan.d/charon/stroke.conf \
    packages/apps/ShannonIms/bin/StrongSwan/conf/strongswan.d/charon/updown.conf:/system/etc/strongswan.d/charon/updown.conf \
    packages/apps/ShannonIms/bin/StrongSwan/conf/strongswan.d/charon/x509.conf:/system/etc/strongswan.d/charon/x509.conf \
    packages/apps/ShannonIms/bin/StrongSwan/conf/strongswan.d/charon/xauth-generic.conf:/system/etc/strongswan.d/charon/xauth-generic.conf \
    packages/apps/ShannonIms/bin/StrongSwan/conf/strongswan.d/charon/xcbc.conf:/system/etc/strongswan.d/charon/xcbc.conf \
    packages/apps/ShannonIms/libs/libePdgJni.so:/system/lib64/libePdgJni.so

# IMS media-jni library that has connection of Stagefright for video calling.
#PRODUCT_COPY_FILES += \
    packages/apps/ShannonIms/libs/libmediaadaptor.so:/system/lib64/libmediaadaptor.so

#$(warning current dir: $(dir $(TARGET_BOARD_INFO_FILE)))
#mptool
#include device/samsung/universal3830/mptool/device.mk
# include $(dir $(TARGET_BOARD_INFO_FILE))mptool/device.mk

# ifeq ($(BOARD_USE_FACTORY_IMAGES), true)
# BOARD_USE_SSCR_TOOLS += slogkit

# endif
# BOARD_USE_SSCR_TOOLS += mptool modem wcn
# $(call inherit-product-if-exists, vendor/samsung_slsi/sscr_tools/device.mk)

# NFC Feature
#$(call inherit-product-if-exists, device/samsung/universal3830/nfc/device-nfc.mk)

#PRODUCT_COPY_FILES += \
	device/samsung/universal3830/exynos-thermal/exynos-thermal.conf:$(TARGET_COPY_OUT_VENDOR)/exynos-thermal.conf \
	device/samsung/universal3830/exynos-thermal/exynos-thermal.env:$(TARGET_COPY_OUT_VENDOR)/exynos-thermal.env

#supplicant configs
ifneq ($(BOARD_WPA_SUPPLICANT_DRIVER),)
CONFIG_DRIVER_$(BOARD_WPA_SUPPLICANT_DRIVER) := y
endif
WPA_SUPPL_DIR = external/wpa_supplicant_8
include $(WPA_SUPPL_DIR)/wpa_supplicant/wpa_supplicant_conf.mk
include $(WPA_SUPPL_DIR)/wpa_supplicant/android.config

# Contacts
PRODUCT_PACKAGES += com.android.contacts_slsi.xml
