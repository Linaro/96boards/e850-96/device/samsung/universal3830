/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define LOG_TAG "ERD3830_PowerHAL"

#include <hardware/hardware.h>
#include <hardware/power.h>

struct erd3830_power_module {
	struct power_module base;
};

static void erd3830_power_init(struct power_module __unused *module) {
}

static void erd3830_power_hint(struct power_module *module, power_hint_t hint, void* __unused data) {

	switch (hint) {
		case POWER_HINT_INTERACTION:
			break;
		case POWER_HINT_LAUNCH:
			break;
		default:
			break;
	}
}

static struct hw_module_methods_t power_module_methods = {
	.open = NULL,
};

struct erd3830_power_module HAL_MODULE_INFO_SYM = {
	.base = {
		.common = {
			.tag = HARDWARE_MODULE_TAG,
			.module_api_version = POWER_MODULE_API_VERSION_0_2,
			.hal_api_version = HARDWARE_HAL_API_VERSION,
			.id = POWER_HARDWARE_MODULE_ID,
			.name = "ERD3830Power HAL",
			.author = "The Android Open Source Project",
			.methods = &power_module_methods,
		},
		.init = erd3830_power_init,
		.powerHint = erd3830_power_hint,
	},
};
