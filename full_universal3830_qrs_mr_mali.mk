# Copyright (C) 2011 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#
# This file is the build configuration for a full Android
# build for universal2100 hardware. This cleanly combines a set of
# device-specific aspects (drivers) with a device-agnostic
# product configuration (apps). Except for a few implementation
# details, it only fundamentally contains two inherit-product
# lines, full and universal2100, hence its name.
#

MALI_ON_KEYSTONE := true

$(call inherit-product, device/samsung/universal3830/full_universal3830_qrs_mr.mk)

PRODUCT_PACKAGES += \
	libmali \
	libOpenCL \
	vulkan.mali

PRODUCT_PROPERTY_OVERRIDES += \
        ro.hardware.vulkan=mali

# Enable protected contents on SurfaceFlinger
#PRODUCT_PROPERTY_OVERRIDES += \
#	ro.surface_flinger.protected_contents=1

PRODUCT_NAME := full_universal3830_qrs_mr_mali
PRODUCT_DEVICE := universal3830_qrs_mr_mali
