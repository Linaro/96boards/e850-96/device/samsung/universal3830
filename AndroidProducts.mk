#
# Copyright (C) 2011 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/full_universal3830_s.mk \
    $(LOCAL_DIR)/full_universal3830_qrs_mr.mk \
    $(LOCAL_DIR)/full_universal3830_rs_mr.mk \
    $(LOCAL_DIR)/full_universal3830_32bit_s.mk \
    $(LOCAL_DIR)/full_universal3830_s_mali.mk \
    $(LOCAL_DIR)/full_universal3830_qrs_mr_mali.mk \
    $(LOCAL_DIR)/full_universal3830_st_mr.mk \
    $(LOCAL_DIR)/full_universal3830_32bit_st_mr.mk \
    $(LOCAL_DIR)/full_universal3830_rst_mr.mk

COMMON_LUNCH_CHOICES := \
	full_universal3830_s-eng \
	full_universal3830_s-userdebug \
	full_universal3830_s-user \
        full_universal3830_qrs_mr-eng \
        full_universal3830_qrs_mr-userdebug \
        full_universal3830_qrs_mr-user \
	full_universal3830_rs_mr-eng \
        full_universal3830_rs_mr-userdebug \
        full_universal3830_rs_mr-user \
	full_universal3830_32bit_s-eng \
	full_universal3830_32bit_s-userdebug \
        full_universal3830_32bit_s-user \
	full_universal3830_st_mr-eng \
	full_universal3830_st_mr-userdebug \
        full_universal3830_st_mr-user \
	full_universal3830_32bit_st_mr-eng \
	full_universal3830_32bit_st_mr-userdebug \
	full_universal3830_32bit_st_mr-user \
	full_universal3830_rst_mr-eng \
	full_universal3830_rst_mr-userdebug \
	full_universal3830_rst_mr-user
