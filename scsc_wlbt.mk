# Copy WLBT fw and other related files from vendor/samsung_slsi/mx140/firmware/
$(call inherit-product-if-exists, vendor/samsung_slsi/mx140/firmware/wlbt_firmware.mk)

# PRODUCT_PACKAGES for vendor/samsung_slsi/scsc_tools/dev_tools/
$(call inherit-product-if-exists, vendor/samsung_slsi/scsc_tools/dev_tools/dev_tools.mk)
