# Copyright (C) 2011 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#
# This file is the build configuration for a full Android
# build for universal850 hardware. This cleanly combines a set of
# device-specific aspects (drivers) with a device-agnostic
# product configuration (apps). Except for a few implementation
# details, it only fundamentally contains two inherit-product
# lines, full and universal850, hence its name.
#

# Build flag to use for Nacho S Launching Products
ENABLE_S_FEATURE := true

# add launcer
PRODUCT_PACKAGES += \
    Launcher3

# Live Wallpapers
PRODUCT_PACKAGES += \
        LiveWallpapers \
        LiveWallpapersPicker \
        MagicSmokeWallpapers \
        VisualizationWallpapers \
        librs_jni

PRODUCT_PROPERTY_OVERRIDES := \
        net.dns1=8.8.8.8 \
        net.dns2=8.8.4.4

# Inherit from those products. Most specific first.
$(call inherit-product, device/samsung/universal3830/device.mk)

#
# ADDITIONAL VENDOR BUILD PROPERTIES (Telephony)
#

TARGET_SOC := exynos850
TARGET_SOC_BASE := exynos3830
TARGET_BOARD_PLATFORM := universal3830
TARGET_BOOTLOADER_BOARD_NAME := exynos850
BOARD_KERNEL_CMDLINE := androidboot.hardware=$(TARGET_SOC)
BOARD_KERNEL_CMDLINE += androidboot.selinux=enforce

PRODUCT_NAME := full_universal3830_st_mr
PRODUCT_DEVICE := universal3830
PRODUCT_BRAND := Android
PRODUCT_MODEL := Full Android on UNIVERSAL850
PRODUCT_MANUFACTURER := Samsung Electronics Co., Ltd.
TARGET_LINUX_KERNEL_VERSION := 4.19

TARGET_BUILD_KERNEL_FROM_SOURCE := true

TARGET_BOARD_SUPPORT_FEATURE := S
